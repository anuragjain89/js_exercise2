var CheckboxSelectUnselectAll = {

  elements: document.querySelectorAll('input[type="checkbox"]'),

  setAllCheckbox: function (setValue) {
    var elements = Array.prototype.slice.call(this.elements);
    var loopVariable = elements.length;
    while (loopVariable--) {
      elements[loopVariable].checked = setValue;
    }
  },

  selectAllCheckbox: function () {
    CheckboxSelectUnselectAll.setAllCheckbox(true);
  },

  unselectAllCheckbox: function () {
    CheckboxSelectUnselectAll.setAllCheckbox(false);
  },

  eventHandler: function () {
    this.document.getElementById('check').addEventListener('click', CheckboxSelectUnselectAll.selectAllCheckbox);
    this.document.getElementById('uncheck').addEventListener('click', CheckboxSelectUnselectAll.unselectAllCheckbox);
  }
};

window.addEventListener('load', CheckboxSelectUnselectAll.eventHandler);